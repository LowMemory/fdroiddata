Categories:Multimedia,Internet
License:GPLv3
Web Site:http://www.radio-browser.info
Source Code:https://github.com/segler-alex/RadioDroid
Issue Tracker:https://github.com/segler-alex/RadioDroid/issues

Auto Name:RadioDroid
Summary:Browse and stream internet radios
Description:
Look up online radio streams at [http://www.radio-browser.info RadioBrowser] and
listen to them. RadioBrowser is a wiki like aproach to collect as much online
radio stations as possible. Every one can help by not only adding new stations
but also tagging existing ones.
.

Repo Type:git
Repo:https://github.com/segler-alex/RadioDroid

Build:0.4,4
    commit=0.4
    subdir=app
    gradle=yes

Build:0.6,6
    commit=0.6
    subdir=app
    gradle=yes

Build:0.7,7
    commit=0.7
    subdir=app
    gradle=yes

Build:0.8,8
    commit=0.8
    subdir=app
    gradle=yes

Build:0.9,9
    commit=0.9
    subdir=app
    gradle=yes

Build:0.9.1,10
    commit=0.9.1
    subdir=app
    gradle=yes

Build:0.10.1,12
    commit=0.10.1
    subdir=app
    gradle=yes

Build:0.11,13
    commit=0.11
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:0.11
Current Version Code:13
