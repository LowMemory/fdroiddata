AntiFeatures:NonFreeAssets
Categories:Games
License:LGPLv2.1+
Author Name:Rubenwardy
Web Site:http://rubenwardy.com/mtmods4android/
Source Code:https://github.com/rubenwardy/mtmods4android
Issue Tracker:https://github.com/rubenwardy/mtmods4android/issues

Auto Name:Minetest Mods
Summary:Minetest Mods manager
Description:
Install and update Minetest mods.

The app depends on a free network service, see
[https://github.com/rubenwardy/mtmods4android_server source code] and
[http://app-mtmm.rubenwardy.com/ website] for more details.

'''AntiFeatures:'''

* non-free assets are used: All resources featuring the Minetest logo are CC-BY-SA-NC, original author is erlehmann. Also CC-BY-3.0 licensed assets from [http://shreyasachar.com/AndroidAssetStudio/attribution.html Android material design asset studio] have been used for app/src/main/res/drawable/ic_public_24dp.xml and app/src/main/res/drawable/ic_report_problem_24dp.xml.
.

Repo Type:git
Repo:https://github.com/rubenwardy/mtmods4android.git

Build:1.2.1,5
    commit=v1.2.1
    subdir=app
    gradle=yes

Build:1.2.2,6
    commit=v1.2.2
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.2.2
Current Version Code:6
