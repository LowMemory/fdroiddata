Categories:Phone & SMS
License:GPLv2
Web Site:
Source Code:https://github.com/yeriomin/SmsScheduler
Issue Tracker:https://github.com/yeriomin/SmsScheduler/issues

Auto Name:SMS Scheduler
Summary:Lets you schedule an sms to be sent at a specific time
Description:
This app is for you if:

* You keep forgetting to send a birthday sms to someone
* You have that friend who doesn't seem to know what an alarm is
.

Repo Type:git
Repo:https://github.com/yeriomin/SmsScheduler

Build:0.1,1
    commit=v0.1
    subdir=app
    gradle=yes

Build:0.2,2
    commit=v0.2
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.2
Current Version Code:2
